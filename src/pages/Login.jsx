import React, { Component } from 'react'
import NavBar from '../components/NavBar';
import TextInput from '../components/TextInput';
import axios from 'axios';

export class Login extends Component {
    state = {
        email: '',
        password: '',
        password_input: 'password',
        login_status: true
    }

    handleSubmit = (e) => {
        e.preventDefault()
        const {email, password} = this.state
        axios.post(`https://sheltered-spire-75633.herokuapp.com/auth/sign_in`, 
            {
                email: email, 
                password: password
            }, 
            {
                headers: {
                    'token-type': null,
                    client: null,
                    uid: null,
                }
            }
        )
        .then(res => {
            localStorage.setItem('access-token', res.headers['access-token']);
            localStorage.setItem('client', res.headers.client);
            localStorage.setItem('uid', res.headers.uid);
            this.props.history.push('/application_management')
        })
        .catch((error) => {
            console.log(error)
            this.setState({login_status: false})
        })
    }

    handleChange = e => {
        this.setState({[e.target.name]: e.target.value })
    }

    changePasswordInput = () => {
        const {password_input} = this.state
        this.setState({password_input: (password_input === 'password') ? 'text' : 'password' })
    }

    render() {
        const {email, password, password_input, login_status} = this.state
        
        return (
            <>
                <NavBar home={false}/>
                <div className="container">
                    <div className="row section">
                        <div className="col s12 m8 offset-m2">
                            <div className="card">
                                <form onSubmit={this.handleSubmit}>
                                    <div className="card-content">
                                        <span className="card-title center-align">Inicio de sesión</span>
                                        <TextInput
                                            name="email"
                                            value={email}
                                            handleChange={this.handleChange}
                                            placeholder="example@telefonica.com.ve"
                                            label="Correo electronico"
                                        />
                                        <div className="row">
                                            <div className="input-field col s12">
                                            <input id='password' placeholder="•••••••••••••••" name='password' onChange={this.handleChange} value={password} type={password_input} className="validate"/>
                                            <i className="material-icons remove-password" onClick={this.changePasswordInput}>remove_red_eye</i>
                                            <label htmlFor='password' className="active">Contraseña</label>
                                            </div>
                                        </div>
                                        {login_status ? <></>: <p className='red-text'>Usuario o Contraseña invalidos.</p>}
                                        <center>
                                            <button type='submit' className='btn form-btn orange'>Entrar</button>
                                        </center>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default Login
