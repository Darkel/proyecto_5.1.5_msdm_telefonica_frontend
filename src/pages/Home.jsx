import React from 'react'
import NavBar from '../components/NavBar'
import Parallax from '../components/Parallax'
import Footer from '../components/Footer'
import Services from '../components/Services'
import Application from '../components/Application'
import MSDM from '../components/MSDM'
import Activities from '../components/Activities'
import Companies from '../components/Companies'

function Home() {
    return (
        <>
            <NavBar home={true}/>
            <div className="container row section">
                <MSDM/>
                <Application/>
            </div>
            <Services/>
            <div className="row section" style={{backgroundColor: "#132A3E"}}>
                <Activities/>
            </div>
            <div className="section companies-separation">
                <Companies/>
            </div>
            <Parallax/>
            <Footer/>
        </>
    )
}

export default Home
