import React, { Component } from 'react'
import NavBar from '../components/ApplicationManagement/NavBar'
import axios from 'axios';
import ApplicationShowCard from '../components/ApplicationManagement/ApplicationShowCard';
import {Link} from 'react-router-dom'

export class ApplicationShow extends Component {
    constructor(props){
        super(props)
        this.state = {
            applications: '',
            loggedIn: this.checkLogin()
        }
    }

    checkLogin = () => {
        if (localStorage.getItem('access-token') && localStorage.getItem('client') && localStorage.getItem('uid')) {
            return true
        } else {
            return false
        }
    }
    

    requestApplications = () => {
        axios.get(`https://sheltered-spire-75633.herokuapp.com/applications`)
        .then(res => {
            this.setState({applications: res.data});
        })
        .catch((error) => {
            console.log(error);
        })
    }
    
    componentDidMount(){
        if(this.state.loggedIn) {
            this.requestApplications()
        } else {
            this.props.history.push('/')
        }
    }

    
    render() {
        const {applications} = this.state

        if (typeof(applications) === 'object') {
            return (
                <div>
                    <NavBar/>
                    <main>
                        <div className="container">
                            <div className="row section">
                                <Link to='/application_management'><button className='btn orange'><i className="material-icons left">arrow_back</i>listado</button></Link>
                                
                                <h3>Solicitud #{this.props.match.params.id}</h3>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo facilis dolorem similique eveniet molestias repudiandae consequuntur natus soluta animi mollitia, sequi suscipit rerum aperiam unde, excepturi quia porro exercitationem! Deserunt?</p>
                                <ApplicationShowCard applications={applications} id={this.props.match.params.id}/>
                            </div>
                        </div>
                    </main>
                </div>
            )
        } else {
            return (
                <div>
                    <NavBar/>
                    <main>
                        <div className="container">
                            <div className="row section">
                            <center>
                                <div className="preloader-wrapper big active" style={{width: 128, height: 128}}>
                                    <div className="spinner-layer spinner-blue-only">
                                        <div className="circle-clipper left">
                                            <div className="circle"></div>
                                        </div>
                                        <div className="gap-patch">
                                            <div className="circle"></div>
                                        </div>
                                        <div className="circle-clipper right">
                                            <div className="circle"></div>
                                        </div>
                                    </div>
                                </div>
                                <h6 className="form-title">Procesando solicitud, por favor, espere un momento...</h6>
                            </center>
                            </div>
                        </div>
                    </main>
                </div>
            )
        }
    }
}

export default ApplicationShow
