import React, { Component } from 'react'
import NavBar from '../components/ApplicationManagement/NavBar'
import '@fullcalendar/core/main.css'
import '../assets/FullCalendar.css'
import axios from 'axios';
import CalendarShow from '../components/Calendar/CalendarShow';

export class ApplicationDashboard extends Component {
    state = {
        applications: ''
    }

    requestApplications = () => {
        axios.get(`https://sheltered-spire-75633.herokuapp.com/applications`)
        .then(res => {
            this.setState({applications: res.data.filter(application => application.application_status === 'Aprobado')});
        })
        .catch((error) => {
            console.log(error);
        })
    }

    componentDidMount(){
        this.requestApplications()
    }

    render() {
        const {applications} = this.state
        if (typeof(applications) === 'object') {
            return (
                <div>
                    <NavBar/>
                    <main>
                        <div className="container">
                            <div className="row section">
                                <CalendarShow applications={this.state.applications}/>
                            </div>
                        </div>
                    </main>
                </div>
            )
        } else {
            return (
                <div>
                    <NavBar/>
                    <main>
                        <div className="container">
                            <div className="row section">
                            <h3>Listado de solicitudes</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo facilis dolorem similique eveniet molestias repudiandae consequuntur natus soluta animi mollitia, sequi suscipit rerum aperiam unde, excepturi quia porro exercitationem! Deserunt?</p>
                            
                            <center>
                                <div className="preloader-wrapper big active" style={{width: 128, height: 128}}>
                                    <div className="spinner-layer spinner-blue-only">
                                        <div className="circle-clipper left">
                                            <div className="circle"></div>
                                        </div>
                                        <div className="gap-patch">
                                            <div className="circle"></div>
                                        </div>
                                        <div className="circle-clipper right">
                                            <div className="circle"></div>
                                        </div>
                                    </div>
                                </div>
                                <h6 className="form-title">Procesando solicitud, por favor, espere un momento...</h6>
                            </center>

                            </div>
                        </div>
                    </main>
                </div>
            )
        }
    }
}

export default ApplicationDashboard
