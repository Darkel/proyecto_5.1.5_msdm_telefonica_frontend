import React, { Component } from 'react';
import NavBar from '../components/NavBar';
import ApplicantForm from '../components/Form/ApplicantForm';
import OrganizationForm from '../components/Form/OrganizationForm';
import ActivityForm from '../components/Form/ActivityForm';
import MediaForm from '../components/Form/MediaForm';
import ReservationForm from '../components/Form/ReservationForm';
import TechnicalForm from '../components/Form/TechnicalForm';
import SurveyForm from '../components/Form/SurveyForm';
import ConfirmationForm from '../components/Form/ConfirmationForm';
import ResultForm from '../components/Form/ResultForm';

class NewApplication extends Component {
    state = {
        step: 1,
        room_type: "",
        usage_type: "",
        start_time: "",
        end_time: "",
        //Applicant
        full_name: "",
        dni: "",
        cellphone: "",
        telephone: "",
        email: "",
        //Organization
        organization_name: "",
        address: "",
        rif: "",
        sector: "",
        position: "",
        //Survey
        public_type: "",
        people: "",
        commentary: "",
        know_type: "",
        know_other: "",
        profile_other: "",
        age_details_attributes: {
            very_young: false,
            young: false,
            teen: false,
            pre_adult: false,
            adult: false,
            old_age: false,
        },
        profile_types_attributes: {
            general: false,
            student: false,
            teacher: false,
            professional: false, 
            community: false,
            researcher: false,
            entrepreneur: false,
            other: false
        },
        //Activity
        activity_name: "",
        objective: "",
        description: "",
        activity_type: "",
        activity_other: "",
        //Advertising
        media: "",
        advertising_types_attributes: {
            television: false,
            radio: false,
            prensa: false,
            redesSociales: false,
            mediosDigitales: false,
            otro: false
        },
        advertising_other: "",
        advertising_status: "0",
        //Tech
        technicals_attributes: {
            computadoras: false,
            smarttv: false,
            proyector: false,
            microfono: false,
            tabletas: false,   
            camaras_web: false,   
            audifonos_con_mic: false,
            internet: false
        },
        computadoras_quantity:"",
        microfono_quantity: "",
        tabletas_quantity: "",
        camaras_web_quantity: "",
        audifonos_con_mic_quantity: "",
        //Reservation
        reservation_dates_attributes: {
            date: ''
        }        
    }

    nextStep = () => {
        const { step } = this.state
        this.setState({
            step: step + 1
        })
    }

    prevStep = () => {
        const { step } = this.state
        this.setState({
            step: step - 1
        })
    }

    handleChange = e => {
        this.setState({[e.target.name]: e.target.value })
    }

    handleCheckboxes = attr => e => {
        const prevValues = this.state[attr]
        this.setState({[attr]: {...prevValues, [e.target.name]: e.target.checked}})
    }

    handleDate = e => {
        this.setState({reservation_dates_attributes: {date: e.value}})
    }

    clearDate = () => {
        this.setState({reservation_dates_attributes: {date: ''}})
    }

    handleTime = (name, value) =>{
        this.setState({[name]: value})
    }

    render() {
        const { step } = this.state
        //Supah God Tier Destructuring.
        const {
            room_type, usage_type, start_time, end_time, reservation_dates_attributes,
            full_name, dni, cellphone, telephone, email,
            organization_name, address, rif, sector, position,
            activity_name, objective, description, activity_other, activity_type,
            advertising_types_attributes, media, advertising_other, advertising_status,
            public_type, people, commentary, know_type, know_other, profile_other, age_details_attributes, profile_types_attributes,
            technicals_attributes, computadoras_quantity, microfono_quantity, tabletas_quantity, camaras_web_quantity, audifonos_con_mic_quantity
        } = this.state
        const applicantValues = { full_name, dni, cellphone, telephone, email }
        const organizationValues = { organization_name, address, rif, sector, position }
        const activityValues = {activity_name, objective, description, activity_other, activity_type}
        const mediaValues = {advertising_types_attributes, media, advertising_other, advertising_status}
        const reservationValues = {room_type, usage_type, start_time, end_time, reservation_dates_attributes}
        const technicalsValues = {technicals_attributes, computadoras_quantity, microfono_quantity, tabletas_quantity, camaras_web_quantity, audifonos_con_mic_quantity}        
        const surveyValues = {public_type, people, commentary, know_type, know_other, profile_other, age_details_attributes, profile_types_attributes}
        
        switch(step) {
            case 1:
                return (
                    <>
                    <NavBar home={false}/>
                    <div className="container row section">
                        <div className="col s12 m8 offset-m2 card">
                            <div className="card-content">
                            <ApplicantForm
                                nextStep = {this.nextStep}
                                handleChange = {this.handleChange}
                                values = {applicantValues}
                            />
                            </div>
                        </div>
                        
                    </div>
                    
                    </>
                );
            case 2:
                return (
                    <>
                    <NavBar home={false}/>
                    <div className="container row section">
                        <div className="col s12 m8 offset-m2 card">
                            <div className="card-content">
                            <OrganizationForm
                                nextStep = {this.nextStep}
                                prevStep = {this.prevStep}
                                handleChange = {this.handleChange}
                                values = {organizationValues}
                            />
                            </div>
                        </div>
                    </div>
                    </>
                );
            case 3:
                return (
                    <>
                    <NavBar home={false}/>
                    <div className="container row section">
                        <div className="col s12 m8 offset-m2 card">
                            <div className="card-content">
                            <ActivityForm
                                nextStep = {this.nextStep}
                                prevStep = {this.prevStep}
                                handleChange = {this.handleChange}
                                values = {activityValues}
                            />
                            </div>
                        </div>
                    </div>
                    </>
                );
            case 4:
                return (
                    <>
                    <NavBar home={false}/>
                    <div className="container row section">
                        <div className="col s12 m8 offset-m2 card">
                            <div className="card-content">
                            <MediaForm
                                nextStep = {this.nextStep}
                                prevStep = {this.prevStep}
                                handleChange = {this.handleChange}
                                handleCheckboxes = {this.handleCheckboxes}
                                values = {mediaValues}
                            />
                            </div>
                        </div>
                    </div>
                    </>
                );
            case 5:
                return (
                    <>
                    <NavBar home={false}/>
                    <div className="container row section">
                        <div className="col s12 m8 offset-m2 card">
                            <div className="card-content">
                            <ReservationForm
                                nextStep = {this.nextStep}
                                prevStep = {this.prevStep}
                                handleChange = {this.handleChange}
                                handleDate = {this.handleDate}
                                handleTime = {this.handleTime}
                                clearDate = {this.clearDate}
                                values = {reservationValues}
                            />
                            </div>
                        </div>
                        <div id="time-picker-container"></div>
                    </div>
                    </>
                );
            case 6:
                return (
                    <>
                    <NavBar home={false}/>
                    <div className="container row section">
                        <div className="col s12 m8 offset-m2 card">
                            <div className="card-content">
                            <TechnicalForm
                                nextStep = {this.nextStep}
                                prevStep = {this.prevStep}
                                handleChange = {this.handleChange}
                                handleCheckboxes = {this.handleCheckboxes}
                                values = {technicalsValues}
                            />
                            </div>
                        </div>
                    </div>
                    </>
                );
            case 7:
                return (
                    <>
                    <NavBar home={false}/>
                    <div className="container row section">
                        <div className="col s12 m8 offset-m2 card">
                            <div className="card-content">
                            <SurveyForm
                                nextStep = {this.nextStep}
                                prevStep = {this.prevStep}
                                handleChange = {this.handleChange}
                                handleCheckboxes = {this.handleCheckboxes}
                                values = {surveyValues}
                            />
                            </div>
                        </div>
                    </div>
                    </>
                );
            case 8:
                return (
                    <>
                    <NavBar home={false}/>
                    <div className="container row section">
                        <div className="col s12 m8 offset-m2 card">
                            <div className="card-content">
                            <ConfirmationForm
                                nextStep = {this.nextStep}
                                prevStep = {this.prevStep}
                                allState = {this.state}
                            />
                            </div>
                        </div>
                    </div>
                    </>
                );
            case 9:
                return (
                    <>
                    <NavBar home={false}/>
                    <div className="container row section">
                        <div className="col s12 m8 offset-m2 card">
                            <div className="card-content">
                            <ResultForm
                                applicantValues = {applicantValues}
                                organizationValues = {organizationValues}
                                activityValues = {activityValues}
                                mediaValues = {mediaValues}
                                reservationValues = {reservationValues}
                                technicalsValues = {technicalsValues}
                                surveyValues = {surveyValues}
                            />
                            </div>
                        </div>
                    </div>
                    </>
                );
            default:
                return (
                    <>Something went, wrong. Like, very wrong.</>
                )
        }

        
    }
}

export default NewApplication;