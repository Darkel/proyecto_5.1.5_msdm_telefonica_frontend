import React from 'react';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'material-design-icons-iconfont'
import 'materialize-css/dist/css/materialize.min.css'
import './assets/App.css';
import {Route} from 'react-router-dom'
import Home from './pages/Home';
import NewApplication from './pages/NewApplication'
import Login from './pages/Login';
import ApplicationManagement from './pages/ApplicationManagement';
import ApplicationShow from './pages/ApplicationShow';
import ApplicationDashboard from './pages/ApplicationDashboard';


function App() {
  return (
    <>
      <Route path="/" exact component={Home}></Route>
      <Route path="/application" exact component={NewApplication}></Route>
      <Route path="/login" exact component={Login}></Route>
      <Route path="/application_management" exact component={ApplicationManagement}></Route>
      <Route path="/application_management/dashboard" exact component={ApplicationDashboard}></Route>
      <Route path="/application_management/show/:id" component={ApplicationShow}></Route>
    </>
  );
}

export default App;
