import React, { Component } from 'react';
import ParallaxImg from '../assets/images/parallax.jpg'
import M from 'materialize-css';

class Parallax extends Component {
    componentDidMount() {
        var elems = document.querySelectorAll('.parallax');
        M.Parallax.init(elems, {});
    }

    render() {
        return (
            <div className="parallax-container">
                <div className="parallax"><img src={ParallaxImg} alt="Parallax"></img></div>
            </div>
        );
    }
}

export default Parallax;