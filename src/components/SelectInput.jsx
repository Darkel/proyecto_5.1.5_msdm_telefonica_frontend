import React, { Component } from 'react'
import M from 'materialize-css';

export class SelectInput extends Component {
    componentDidMount() {
        var elems = document.querySelectorAll('select');
        M.FormSelect.init(elems, {})
    }

    render() {
        const {name, values, value, handleChange, label} = this.props
        const options = values.map( (value, index) => {
            return (
                <option key={index} value={value}>{value}</option>
            )
        })

        return (
            <div className="row">
                <div className="input-field col s12">
                <select id={name} name={name} onChange={handleChange} value={value}>
                    <option value="" disabled>Elije un tipo de actividad.</option>
                    {options}
                </select>
                <label htmlFor={name}>{label}</label>
                </div>
            </div>
        )
    }
}

export default SelectInput
