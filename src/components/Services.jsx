import React from 'react'

export default function Services() {
    return (
        <div id="services" className="row container section" style={{borderTop: "1px solid rgba(160,160,160,0.3)"}}>
            <div className="col s12">
                <div className="service-title center-align">
                    <h3>Usos de la Mega Sala Digital Movistar</h3>
                    <h5 className="grey-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit.</h5>
                </div>
                <div className="services-container">
                    <div className="services-item">
                        <i className="large material-icons light-blue-text">school</i>
                        <h5>Lorem Ipsum</h5> 
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias omnis quisquam ipsum saepe laudantium quam.</p>
                    </div>
                    <div className="services-item">
                            <i className="large material-icons light-blue-text">laptop</i>
                        <h5>Lorem Ipsum</h5> 
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias omnis quisquam ipsum saepe laudantium quam.</p>
                    </div>
                    <div className="services-item">
                            <i className="large material-icons light-blue-text">language</i>
                        <h5>Lorem Ipsum</h5> 
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias omnis quisquam ipsum saepe laudantium quam.</p>
                    </div>
                    <div className="services-item">
                            <i className="large material-icons light-blue-text">present_to_all</i>
                        <h5>Lorem Ipsum</h5> 
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias omnis quisquam ipsum saepe laudantium quam.</p>
                    </div>
                    <div className="services-item">
                            <i className="large material-icons light-blue-text">important_devices</i>
                        <h5>Lorem Ipsum</h5> 
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias omnis quisquam ipsum saepe laudantium quam.</p>
                    </div>
                    <div className="services-item">
                            <i className="large material-icons light-blue-text">panorama_horizontal</i>
                        <h5>Lorem Ipsum</h5> 
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Alias omnis quisquam ipsum saepe laudantium quam.</p>
                    </div>
                </div>
            </div>
        </div>
    )
}
