import React, { Component } from 'react'

export class CheckboxInput extends Component {
    render() {
        const {name, value, handleCheckboxes, label, disabled} = this.props
        
        return (
            <div className="form-radio">
                <label>
                    <input type="checkbox" name={name} onChange={handleCheckboxes} checked={value} disabled={disabled}/>
                    <span>{label}</span>
                </label>
            </div>
        )
    }
}

export default CheckboxInput
