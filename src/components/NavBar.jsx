import React, {useEffect} from 'react'
import Logo from '../assets/images/logo.png'
import {Link} from 'react-router-dom'
import M from 'materialize-css';

function NavBar({home}) {
    useEffect(() => {
        var elems = document.querySelectorAll('.sidenav');
        M.Sidenav.init(elems, {});
    }, []);

    if (home){
        return (
            <>
            <nav style={{height:"96px", lineHeight:"96px"}}>
                <div className="nav-wrapper" style={{backgroundColor: "#132A3E"}}>
                <Link to="/"><img className="brand-logo logo-img" src={Logo} alt="Logo"/></Link>
                
                <a href="#!" data-target="slide-out" className="sidenav-trigger"><i className="material-icons" style={{lineHeight: '96px', fontSize: 40}}>menu</i></a>
                <ul id="nav-mobile" className="right hide-on-med-and-down">
                    <li><a href="#msdm">MSDM</a></li>
                    <li><a href="#services">Servicios</a></li>
                    <li><a href="#activities">Actividades</a></li>
                    <li><a href="#companies">Compañias</a></li>
                    <li><Link to='/login' className="waves-effect waves-light btn orange"><i className="material-icons left">input</i>Login</Link></li>
                </ul>
                </div>
            </nav>
            <ul id="slide-out" className="sidenav">
                <li><a className='sidenav-close' href="#msdm">MSDM</a></li>
                <li><a className='sidenav-close' href="#services">Servicios</a></li>
                <li><a className='sidenav-close' href="#activities">Actividades</a></li>
                <li><a className='sidenav-close' href="#companies">Compañias</a></li>
                <li><Link to='/login' className="waves-effect waves-light btn orange sidenav-close"><i className="material-icons left">input</i>Login</Link></li>
            </ul>
            </>
        )
    } else {
        return (
            <nav style={{height:"96px", lineHeight:"96px"}}>
                <div className="nav-wrapper" style={{backgroundColor: "#132A3E"}}>
                <Link to="/"><img className="logo-img brand-logo" src={Logo} alt="Logo"/></Link>
                </div>
            </nav>
        )
    }
    
}

export default NavBar
