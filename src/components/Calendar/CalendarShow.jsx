import React, { Component } from 'react'
import { Calendar } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import {Redirect} from 'react-router-dom'

export class CalendarShow extends Component {
    state ={
        redirect: false,
        activityId: ''
    }

    componentDidMount(){
        const {applications} = this.props
        const events = []
        for (const application in applications) {
            const dates = applications[application].reservation_dates;
            for (const date in dates) {
                var event = {   
                    id: applications[application].id,
                    title: applications[application].activity.name,
                    start: dates[date].date
                }
                events.push(event)
            }
        }

        var calendarEl = document.getElementById('calendar');

        var calendar = new Calendar(calendarEl, {
            plugins: [ dayGridPlugin ],
            defaultView: 'dayGridMonth',
            events: events,
            eventClick: ((info)=>{
                this.setState({activityId: info.event.id})
                this.setState({redirect: true})
            })
        });

        calendar.render();
    }

    renderRedirect = () => {
        if (this.state.redirect) {
          return <Redirect to={`/application_management/show/${this.state.activityId}`} />
        }
      }

    render() {
        return (
            <>
                {this.renderRedirect()}
                <div id="calendar"></div>
            </>
        )
    }
}

export default CalendarShow
