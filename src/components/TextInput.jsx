import React, { Component } from 'react'

export class TextInput extends Component {
    render() {
        const {name, value, handleChange, placeholder, label, disabled} = this.props
        return (
            <div className="row">
                <div className="input-field col s12">
                <input id={name} placeholder={placeholder} name={name} onChange={handleChange} value={value} type="text" className="validate" disabled={disabled}/>
                <label htmlFor={name} className="active">{label}</label>
                </div>
            </div>
        )
    }
}

export default TextInput
