import React, { Component } from 'react';
import Image from '../assets/images/msdm730.jpg'

class MSDM extends Component {
    render() {
        return (
            <div id="msdm" className="col s12 l7">
                <div className="card">
                    <div className="card-image">
                    <img src={Image} alt="msdm730"/>
                    <span className="card-title cyan-text card-title-msdm">Mega Sala Digital Movistar</span>
                    </div>
                    <div className="card-content">
                    <p>La Mega Sala Digital Movistar forma parte del proyecto Salas Digitales Movistar, una iniciativa social emprendida con el objetivo de impulsar la educación digital en la población venezolana, especialmente en las comunidades más vulnerables contribuyendo a mejorar sus vidas por medio del uso y aprovechamiento de las TIC.</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default MSDM;