import React, { Component } from 'react'
import M from 'materialize-css';
import moment from 'moment';
import Auditorio from '../../assets/images/sala auditorio.png'
import Combinada from '../../assets/images/sala combinada.jpg'
import Computacion from '../../assets/images/sala computacion.png'
import Logo from '../../assets/images/logo.png'
import {Calendar} from 'primereact/calendar';
import axios from 'axios';

export class ReservationForm extends Component {
    constructor(props) {
        super(props);
        this.start_time = React.createRef();
        this.end_time = React.createRef();
    }

    state = {
        reservation_type: "",
        morning_days: [],
        afternoon_days: [],
        allday_days: []
    }

    handleReservation = e => {
        this.props.clearDate()
        this.setState({reservation_type: e.target.value })
    }

    handleTime = () => {
        const start_time = this.start_time.current.value
        const end_time = this.end_time.current.value
        this.props.handleTime('start_time', start_time)
        this.props.handleTime('end_time', end_time)
    }

    handleRoom = () => {
        const {room_type} = this.props.values
        if (room_type === 'Sala de computación'){
            return Computacion         
        } else if (room_type === 'Sala tipo auditorio'){
            return Auditorio
        } else if (room_type === 'Ambas salas'){
            return Combinada
        } else {
            return Logo
        }
    }

    requestDates = () => {
        axios.get(`https://sheltered-spire-75633.herokuapp.com/application_management/days`)
        .then(res => {
            const morning_days = []
            const afternoon_days = []
            const allday_days = [] 
            for (const index in res.data[0]) {
                const date = res.data[0][index];
                morning_days.push(new Date(moment(date)))
            }

            for (const index in res.data[1]) {
                const date = res.data[1][index];
                afternoon_days.push(new Date(moment(date)))
            }

            for (const index in res.data[2]) {
                const date = res.data[2][index];
                allday_days.push(new Date(moment(date)))
            }
            this.setState({morning_days: morning_days})
            this.setState({afternoon_days: afternoon_days})
            this.setState({allday_days: allday_days})
        })
        .catch((error) => {
            console.log(error);
        })
    }
 
    componentDidMount() {
        this.requestDates()
        var timepicker = document.querySelectorAll('.timepicker');
        M.Timepicker.init(timepicker, {
            container: '#time-picker-container',
            onCloseEnd: this.handleTime
        });
        var materialBox = document.querySelectorAll('.materialboxed');
        M.Materialbox.init(materialBox, {});
    }

    render() {
        // Variables for Date Input.
        var dateMin = new Date(moment().add(15, 'days').toString())
        var dateMax = new Date(moment().add(6, 'months').toString())
        let es = {
            firstDayOfWeek: 0,
            dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
            dayNamesShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
            dayNamesMin: ["D", "L", "M", "X", "J", "V", "S"],
            monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthNamesShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
            today: 'Hoy',
            clear: 'Limpiar',
            dateFormat: 'dd/mm/yy',
            weekHeader: 'Sm'
        };

        const {handleChange, prevStep, nextStep, handleDate} = this.props
        const {room_type, usage_type, start_time, end_time, reservation_dates_attributes} = this.props.values
        const {reservation_type, morning_days, afternoon_days, allday_days} = this.state
        const roomPicture = this.handleRoom()
        return (
            <>
                <span className="card-title center-align">Uso y reserva de fecha</span>
                <h6 className="form-title">Tipo de sala a usar:</h6>
                <div className="form-radio">
                    <label>
                        <input className="with-gap" name="room_type" type="radio" onChange={handleChange} checked={room_type === 'Sala de computación'} value="Sala de computación"/>
                        <span>Sala de computacion</span>
                    </label>
                </div>
                <div className="form-radio">
                    <label>
                        <input className="with-gap" name="room_type" type="radio" onChange={handleChange} checked={room_type === 'Sala tipo auditorio'} value="Sala tipo auditorio"/>
                        <span>Sala tipo auditorio</span>
                    </label>
                </div>
                <div className="form-radio">
                    <label>
                        <input className="with-gap" name="room_type" type="radio" onChange={handleChange} checked={room_type === 'Ambas salas'} value="Ambas salas"/>
                        <span>Ambas salas</span>
                    </label>
                {/* Picture */}
                <img className="materialboxed" src={roomPicture} width='150' alt='Foto de la Sala' style={{position: 'absolute', right: 24, top: 72}}></img>
                </div>
                
                <h6 className="form-title">Periodo de uso de la sala</h6>
                <div className="form-radio">
                    <label>
                        <input className="with-gap" name="reservation_type" type="radio" onChange={this.handleReservation} checked={reservation_type === 'Actividad Unica'} value="Actividad Unica"/>
                        <span>Actividad Unica</span>
                    </label>
                </div>
                <div className="form-radio">
                    <label>
                        <input className="with-gap" name="reservation_type" type="radio" onChange={this.handleReservation} checked={reservation_type === 'Actividad Periodica'} value="Actividad Periodica"/>
                        <span>Actividad Periodica</span>
                    </label>
                </div>
                <h6 className="form-title">Bloque horario</h6>
                <div className="form-radio">
                    <label>
                        <input className="with-gap" name="usage_type" type="radio" onChange={handleChange} checked={usage_type === '8:00 am a 12:00 pm'} value="8:00 am a 12:00 pm"/>
                        <span>8:00 am a 12:00 pm</span>
                    </label>
                </div>
                <div className="form-radio">
                    <label>
                        <input className="with-gap" name="usage_type" type="radio" onChange={handleChange} checked={usage_type === '1:00 pm a 4:00 pm'} value="1:00 pm a 4:00 pm"/>
                        <span>1:00 pm a 4:00 pm</span>
                    </label>
                </div>
                <div className="form-radio">
                    <label>
                        <input className="with-gap" name="usage_type" type="radio" onChange={handleChange} checked={usage_type === 'Todo el día'} value="Todo el día"/>
                        <span>Todo el día</span>
                    </label>
                </div>
                <h6 className="form-title">Dia y Hora</h6>
                <div className="row">
                    <div className="input-field col s12">
                        <i className="material-icons small" style={{position: "absolute", right: 16}}>event</i>
                        { usage_type ? <> </> : <input disabled value="Seleccione un bloque horario" id="disabled" type="text"/>}
                        { usage_type === 'Todo el día' ? 
                            <Calendar  
                                id='reservation_dates'
                                locale={es}
                                minDate={dateMin} 
                                maxDate={dateMax} 
                                disabledDates={allday_days}
                                disabledDays={[0,6]}
                                value={reservation_dates_attributes.date} 
                                onChange={handleDate} 
                                selectionMode={reservation_type === 'Actividad Unica' ? "single" : "multiple"} 
                            />
                            :
                            <></>
                        }
                        { usage_type === '8:00 am a 12:00 pm' ? 
                            <Calendar  
                                id='reservation_dates'
                                locale={es}
                                minDate={dateMin} 
                                maxDate={dateMax} 
                                disabledDates={morning_days}
                                disabledDays={[0,6]}
                                value={reservation_dates_attributes.date} 
                                onChange={handleDate} 
                                selectionMode={reservation_type === 'Actividad Unica' ? "single" : "multiple"} 
                            />
                            :
                            <> </>
                        }
                        { usage_type === '1:00 pm a 4:00 pm' ? 
                            <Calendar  
                                id='reservation_dates'
                                locale={es}
                                minDate={dateMin} 
                                maxDate={dateMax} 
                                disabledDates={afternoon_days}
                                disabledDays={[0,6]}
                                value={reservation_dates_attributes.date} 
                                onChange={handleDate} 
                                selectionMode={reservation_type === 'Actividad Unica' ? "single" : "multiple"} 
                            />
                            :
                            <> </>
                        }
                        <label htmlFor="reservation_dates" className="active">Fecha de la actividad</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                    <i className="material-icons prefix">access_time</i>
                    <input id="start_time" name="start_time" type="text" defaultValue={start_time} placeholder="08:00 AM" className="timepicker" ref={this.start_time}/>
                    <label htmlFor="start_time" className="active">Hora de inicio prevista</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                    <i className="material-icons prefix">access_time</i>
                    <input id="end_time" name="end_time" type="text" defaultValue={end_time} placeholder="12:00 PM" className="timepicker" ref={this.end_time}/>
                    <label htmlFor="end_time" className="active">Hora de cierre prevista</label>
                    </div>
                </div>
                <div className="form-buttons">
                    <button className='btn form-btn orange' onClick={nextStep}>Siguiente</button>
                    <button className='btn form-btn orange' onClick={prevStep}>Atras</button>
                </div>
            </>
        )
    }
}

export default ReservationForm
