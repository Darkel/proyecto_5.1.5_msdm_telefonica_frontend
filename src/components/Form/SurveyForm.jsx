import React, { Component } from 'react'
import CheckboxInput from '../CheckboxInput'
import TextInput from '../TextInput';

export class SurveyForm extends Component {    
    state = {
        terms: false
    }

    handleTerm = e =>{
        this.setState({terms: e.target.checked})
    }
    render() {

        const {handleChange, handleCheckboxes, nextStep, prevStep} = this.props
        const {public_type, people, commentary, know_type, know_other, profile_other} = this.props.values
        const {very_young, young, teen, pre_adult, adult, old_age} = this.props.values.age_details_attributes
        const {general, student, teacher, professional, community, researcher, entrepreneur, other} = this.props.values.profile_types_attributes
        const {terms} = this.state

        const know_other_input = (know_type === 'Otro') ? <TextInput
            name="know_other"
            value={know_other}
            handleChange={handleChange}
            placeholder="Otro"
            label="Otro medio por el cual conocio la Mega Sala"
        /> : <></>

        const profile_other_input = (other) ? <TextInput
            name="profile_other"
            value={profile_other}
            handleChange={handleChange}
            placeholder="Otro perfil"
            label="Otro perfil de participantes"
        /> : <></>
            
        return (
            <>
            <span className="card-title center-align">Informacion adicional</span>
            <h6 className="form-title">¿Como se entero de la Mega Sala Digital?</h6>
            <div className="form-radio">
                <label>
                    <input className="with-gap" name="know_type" value="Evento de Fundación Telefónica" type="radio" checked={know_type === 'Evento de Fundación Telefónica'} onChange={handleChange} />
                    <span>Evento de Fundación Telefónica</span>
                </label>
            </div>
            <div className="form-radio">
                <label>
                    <input className="with-gap" name="know_type" value="Medios de comunicación tradicionales (TV/Radio)" type="radio" checked={know_type === 'Medios de comunicación tradicionales (TV/Radio)'} onChange={handleChange} />
                    <span>Medios de comunicación tradicionales (TV/Radio)</span>
                </label>
            </div>
            <div className="form-radio">
                <label>
                    <input className="with-gap" name="know_type" value="Medios de comunicación digitales (Redes sociales)" type="radio" checked={know_type === 'Medios de comunicación digitales (Redes sociales)'} onChange={handleChange} />
                    <span>Medios de comunicación digitales (Redes sociales)</span>
                </label>
            </div>
            <div className="form-radio">
                <label>
                    <input className="with-gap" name="know_type" value="Por referencia de otra persona" type="radio" checked={know_type === 'Por referencia de otra persona'} onChange={handleChange} />
                    <span>Por referencia de otra persona</span>
                </label>
            </div>
            <div className="form-radio">
                <label>
                    <input className="with-gap" name="know_type" value="Otro" type="radio" checked={know_type === 'Otro'} onChange={handleChange} />
                    <span>Otro</span>
                </label>
            </div>
            {know_other_input}
            <TextInput
                name="people"
                value={people}
                handleChange={handleChange}
                placeholder="80"
                label="Numero de personas convocadas *"
            />
            <h6 className="form-title">Evento abierto al publico</h6>
            <div className="form-radio">
                <label>
                    <input className="with-gap" name="public_type" value="1" type="radio" checked={public_type === '1'} onChange={handleChange} />
                    <span>Si, actividad publica</span>
                </label>
            </div>
            <div className="form-radio">
                <label>
                    <input className="with-gap" name="public_type" value="0" type="radio" checked={public_type === '0'} onChange={handleChange} />
                    <span>No, actividad privada</span>
                </label>
            </div>

            <h6 className="form-title">Edades de los participantes</h6>
            <CheckboxInput
                name="very_young"
                value={very_young}
                handleCheckboxes={handleCheckboxes('age_details_attributes')}
                label="3 - 6 años"
            />
            <CheckboxInput
                name="young"
                value={young}
                handleCheckboxes={handleCheckboxes('age_details_attributes')}
                label="7 - 12 años"
            />
            <CheckboxInput
                name="teen"
                value={teen}
                handleCheckboxes={handleCheckboxes('age_details_attributes')}
                label="13 - 17 años"
            />
            <CheckboxInput
                name="pre_adult"
                value={pre_adult}
                handleCheckboxes={handleCheckboxes('age_details_attributes')}
                label="18 - 28 años"
            />
            <CheckboxInput
                name="adult"
                value={adult}
                handleCheckboxes={handleCheckboxes('age_details_attributes')}
                label="29 - 55 años"
            />
            <CheckboxInput
                name="old_age"
                value={old_age}
                handleCheckboxes={handleCheckboxes('age_details_attributes')}
                label="55 - > años"
            />

            <h6 className="form-title">Perfil de los participantes</h6>
            <CheckboxInput
                name="general"
                value={general}
                handleCheckboxes={handleCheckboxes('profile_types_attributes')}
                label="Público en general"
            />
            <CheckboxInput
                name="student"
                value={student}
                handleCheckboxes={handleCheckboxes('profile_types_attributes')}
                label="Estudiantes"
            />
            <CheckboxInput
                name="teacher"
                value={teacher}
                handleCheckboxes={handleCheckboxes('profile_types_attributes')}
                label="Educadores"
            />
            <CheckboxInput
                name="professional"
                value={professional}
                handleCheckboxes={handleCheckboxes('profile_types_attributes')}
                label="Profesionales"
            />
            <CheckboxInput
                name="community"
                value={community}
                handleCheckboxes={handleCheckboxes('profile_types_attributes')}
                label="Comunidades"
            />
            <CheckboxInput
                name="researcher"
                value={researcher}
                handleCheckboxes={handleCheckboxes('profile_types_attributes')}
                label="Investigadores"
            />
            <CheckboxInput
                name="entrepreneur"
                value={entrepreneur}
                handleCheckboxes={handleCheckboxes('profile_types_attributes')}
                label="Emprendedores"
            />
            <CheckboxInput
                name="other"
                value={other}
                handleCheckboxes={handleCheckboxes('profile_types_attributes')}
                label="Otro"
            />
            {profile_other_input}
            <div className="row">
                <div className="input-field col s12">
                <textarea id="commentary" name="commentary" placeholder="Comentarios adicionales" onChange={handleChange} defaultValue={commentary} className="materialize-textarea" data-length="300"></textarea>
                <label htmlFor="commentary" className="active">Comentarios adicionales</label>
                </div>
            </div>

            <h6 className="form-title">El solicitante conoce y ha leído los términos de uso de la Mega Sala Movistar antes de llenar esta solicitud y está de acuerdo con los mismos.</h6>
            <div className="form-radio">
                <label>
                    <input type="checkbox" name='terms' checked={terms} onChange={this.handleTerm} />
                    <span>Si</span>
                </label>
            </div>
            <div className="form-buttons">
                <button className={terms ? 'btn form-btn orange' : 'btn form-btn orange disabled'} onClick={nextStep}>Siguiente</button>
                <button className='btn form-btn orange' onClick={prevStep}>Atras</button>
            </div>
            </>
        )
    }
}

export default SurveyForm
