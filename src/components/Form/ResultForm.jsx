import React, { Component } from 'react'
import axios from 'axios';
import {Link} from 'react-router-dom'
import moment from 'moment';

export class SuccessForm extends Component {
    state = {
        response: ''
    }

    handleSubmit = () => {
        const {full_name, dni, cellphone, telephone, email} = this.props.applicantValues
        const { organization_name, address, rif, sector, position } = this.props.organizationValues
        const {activity_name, objective, description, activity_other, activity_type} = this.props.activityValues
        const {advertising_types_attributes, media, advertising_other, advertising_status} = this.props.mediaValues
        const {room_type, usage_type, start_time, end_time, reservation_dates_attributes} = this.props.reservationValues
        const {technicals_attributes, computadoras_quantity, microfono_quantity, tabletas_quantity, camaras_web_quantity, audifonos_con_mic_quantity} = this.props.technicalsValues     
        const {public_type, people, commentary, know_type, know_other, profile_other, age_details_attributes, profile_types_attributes} = this.props.surveyValues

        //profile_types
        const {very_young, young, teen, pre_adult, adult, old_age} = age_details_attributes
        const {general, student, teacher, professional, community, researcher, entrepreneur, other} = profile_types_attributes

        const age_details = []
        if (very_young){
            age_details.push({age_range: '3 - 6 años'})
        }
        if (young){
            age_details.push({age_range: '7 - 12 años'})
        }
        if (teen){
            age_details.push({age_range: '13 - 17 años'})
        }
        if (pre_adult){
            age_details.push({age_range: '18 - 28 años'})
        }
        if (adult){
            age_details.push({age_range: '29 - 55 años'})
        }
        if (old_age){
            age_details.push({age_range: '55 - > años'})
        }

        const profile_types = []
        if (general){
            profile_types.push({name: 'Público en general'})
        }
        if (student){
            profile_types.push({name: 'Estudiantes'})
        }
        if (teacher){
            profile_types.push({name: 'Educadores'})
        }
        if (professional){
            profile_types.push({name: 'Profesionales'})
        }
        if (community){
            profile_types.push({name: 'Comunidades'})
        }
        if (researcher){
            profile_types.push({name: 'Investigadores'})
        }
        if (entrepreneur){
            profile_types.push({name: 'Emprendedores'})
        }


        // media
        const {television, radio, prensa, redesSociales, mediosDigitales, otro} = advertising_types_attributes

        const advertising_types = []
        if (television){
            advertising_types.push({name: 'Televisión'})
        }
        if (radio){
            advertising_types.push({name: 'Radio'})
        }
        if (prensa){
            advertising_types.push({name: 'Prensa'})
        }
        if (redesSociales){
            advertising_types.push({name: 'Redes sociales'})
        }
        if (mediosDigitales){
            advertising_types.push({name: 'Medios digitales'})
        }

        //Tech stuff
        const {computadoras, smarttv, proyector, microfono, tabletas, camaras_web, audifonos_con_mic, internet} = technicals_attributes
        const technicals_types = []
        if (computadoras){
            technicals_types.push({name: 'PC/Laptop', quantity: computadoras_quantity})
        }
        if (smarttv){
            technicals_types.push({name: 'SmartTV / Servicio TV Movistar', quantity: ''})
        }
        if (proyector){
            technicals_types.push({name: 'Proyector de video', quantity: ''})
        }
        if (microfono){
            technicals_types.push({name: 'Sonido / Microfono', quantity: microfono_quantity})
        }
        if (tabletas){
            technicals_types.push({name: 'Tabletas', quantity: tabletas_quantity})
        }
        if (camaras_web){
            technicals_types.push({name: 'Camaras web', quantity: camaras_web_quantity})
        }
        if (audifonos_con_mic){
            technicals_types.push({name: 'Audifonos con microfono', quantity: audifonos_con_mic_quantity})
        }
        if (internet){
            technicals_types.push({name: 'Conectividad a internet', quantity: ''})
        }

        // Reservation Dates.
        const date_json = []
        if (reservation_dates_attributes.date[0] !== undefined) {
            for (const i in reservation_dates_attributes.date ) {
                date_json.push({date: moment(reservation_dates_attributes.date[i]).format('YYYY-MM-DD')})
            }
        } else {
            date_json.push({date: moment(reservation_dates_attributes.date).format('YYYY-MM-DD')})
        }

        const jsonRequest = {
            application: {
                room_type: room_type,
                usage_type: usage_type,
                start_time: start_time,
                end_time: end_time,
                advertising_status: advertising_status,
                applicant_attributes: {
                    full_name: full_name,
                    dni: dni,
                    cellphone: cellphone,
                    telephone: telephone,
                    email: email
                },
                organization_attributes: {
                    name: organization_name,
                    address: address,
                    rif: rif,
                    sector: sector,
                    position: position
                },
                survey_attributes: {
                    public: public_type,
                    people: people,
                    commentary: commentary,
                    know_type: know_type,
                    know_other: (know_type === 'Otro') ? know_other : '',
                    profile_other: (other) ? profile_other : '',
                    age_details_attributes: age_details,
                    profile_types_attributes: profile_types
                },
                activity_attributes: {
                    name: activity_name,
                    objective: objective,
                    description: description,
                    activity_type: activity_type,
                    activity_other: (activity_type === 'Otro') ? activity_other : '' 
                },
                advertising_attributes: {
                    advertising_other: otro ? advertising_other : '',
                    media: media,
                    advertising_types_attributes: advertising_types
                },
                technicals_attributes: technicals_types,
                reservation_dates_attributes: date_json
            }
        }

        axios.post(`https://sheltered-spire-75633.herokuapp.com/applications`, jsonRequest)
        .then(res => {
            console.log(res);
            console.log(res.data);
            this.setState({response: true})
        })
        .catch((error) => {
            console.log(error);
            this.setState({response: false})
        })
    }

    componentDidMount(){
        this.handleSubmit()
    }


    render() {
        const {response} = this.state
        
        if (response === '') {
            return (
                <center>
                    <div className="preloader-wrapper big active" style={{width: 128, height: 128}}>
                        <div className="spinner-layer spinner-blue-only">
                            <div className="circle-clipper left">
                                <div className="circle"></div>
                            </div>
                            <div className="gap-patch">
                                <div className="circle"></div>
                            </div>
                            <div className="circle-clipper right">
                                <div className="circle"></div>
                            </div>
                        </div>
                    </div>
                    <h6 className="form-title">Procesando solicitud, por favor, espere un momento...</h6>
                </center>
            )
        } else if (response) {
            return (
                <center>
                    <i className='material-icons green-text' style={{fontSize: 128}}>check_circle_outline</i>
                    <h6 className="form-title">Su solicitud se ha enviado con exito!</h6>
                    <h6 className="form-title">En estos momentos, el equipo Movistar le notificara por correo si su solicitud ha sido aprobada.</h6>
                    <Link to="/"><button className='btn form-btn orange' href='/'><i className='material-icons left'>home</i>Regresar</button></Link>
                </center>
            )
        } else {
            return (
                <center>
                    <i className='material-icons red-text' style={{fontSize: 128}}>error_outline</i>
                    <h6 className="form-title">Su solicitud no ha podido ser procesada.</h6>
                    <h6 className="form-title">Lamentablemente, no sabemos cual fue el error, debido al que el programador que desarrollo esto le dio pereza hacerlo.</h6>
                    <Link to="/"><button className='btn form-btn orange' href='/'><i className='material-icons left'>home</i>Regresar</button></Link>
                </center>
            )
        }
    }
}

export default SuccessForm
