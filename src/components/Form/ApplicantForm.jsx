import React, { Component } from 'react';
import TextInput from '../TextInput';

class ApplicantForm extends Component {

    render() {
        const {handleChange, nextStep} = this.props
        const {cellphone, dni, email, full_name, telephone} = this.props.values
        return (
            <>
            <span className="card-title center-align">Datos del solicitante</span>
            <TextInput
                name="full_name"
                value={full_name}
                handleChange={handleChange}
                placeholder="Luis Moncada"
                label="Nombre y apellido *"
            />
            <TextInput
                name="dni"
                value={dni}
                handleChange={handleChange}
                placeholder="V-23.456.789"
                label="Cedula de identidad"
            />
            <TextInput
                name="cellphone"
                value={cellphone}
                handleChange={handleChange}
                placeholder="0424-000-0000"
                label="Numero de télefono (Movil)"
            />
            <TextInput
                name="telephone"
                value={telephone}
                handleChange={handleChange}
                placeholder="0212-000-0000"
                label="Numero de télefono (Fijo)"
            />
            <TextInput
                name="email"
                value={email}
                handleChange={handleChange}
                placeholder="fundacion@telefonica.com"
                label="Correo Electronico *"
            />
            <div className="form-buttons">
                <button className='btn form-btn orange ' onClick={nextStep}>Siguiente</button>
            </div>
            </>
        );
    }
}

export default ApplicantForm;