import React, { Component } from 'react'
import CheckboxInput from '../CheckboxInput'
import TextInput from '../TextInput';

export class TechnicalForm extends Component {    
    render() {
        const {handleChange, handleCheckboxes, nextStep, prevStep} = this.props
        const {computadoras_quantity, microfono_quantity, tabletas_quantity, camaras_web_quantity, audifonos_con_mic_quantity} = this.props.values
        const {computadoras, smarttv, proyector, microfono, tabletas, camaras_web, audifonos_con_mic, internet} = this.props.values.technicals_attributes

        const computadoras_input = (computadoras) ? <TextInput
            name="computadoras_quantity"
            value={computadoras_quantity}
            handleChange={handleChange}
            placeholder="Cantidad de computadoras o laptops necesarias"
        /> : <></>

        const microfono_quantity_input = (microfono) ? <TextInput
            name="microfono_quantity"
            value={microfono_quantity}
            handleChange={handleChange}
            placeholder="Cantidad de microfonos"
        /> : <></>

        const tabletas_input = (tabletas) ? <TextInput
            name="tabletas_quantity"
            value={tabletas_quantity}
            handleChange={handleChange}
            placeholder="Cantidad de tabletas"
        /> : <></>

        const camera_web_input = (camaras_web) ? <TextInput
            name="camaras_web_quantity"
            value={camaras_web_quantity}
            handleChange={handleChange}
            placeholder="Cantidad de camaras web"
        /> : <></>

        const audifonos_input = (audifonos_con_mic) ? <TextInput
            name="audifonos_con_mic_quantity"
            value={audifonos_con_mic_quantity}
            handleChange={handleChange}
            placeholder="Cantidad de audifonos"
        /> : <></>
            
        return (
            <>
            <span className="card-title center-align">Requerimientos técnicos</span>            
            <h6 className="form-title">Requerimientos técnicos</h6>
            <CheckboxInput
                name="computadoras"
                value={computadoras}
                handleCheckboxes={handleCheckboxes('technicals_attributes')}
                label="PC/Laptop"
                disabled={false}
            />
            {computadoras_input}
            <CheckboxInput
                name="smarttv"
                value={smarttv}
                handleCheckboxes={handleCheckboxes('technicals_attributes')}
                label="SmartTV / Servicio TV Movistar"
                disabled={false}
            />
            <CheckboxInput
                name="proyector"
                value={proyector}
                handleCheckboxes={handleCheckboxes('technicals_attributes')}
                label="Proyector de video"
                disabled={false}
            />
            <CheckboxInput
                name="microfono"
                value={microfono}
                handleCheckboxes={handleCheckboxes('technicals_attributes')}
                label="Sonido / Microfono"
                disabled={false}
            />
            {microfono_quantity_input}
            <CheckboxInput
                name="tabletas"
                value={tabletas}
                handleCheckboxes={handleCheckboxes('technicals_attributes')}
                label="Tabletas"
                disabled={false}
            />
            {tabletas_input}
            <CheckboxInput
                name="camaras_web"
                value={camaras_web}
                handleCheckboxes={handleCheckboxes('technicals_attributes')}
                label="Camaras web"
                disabled={false}
            />
            {camera_web_input}
            <CheckboxInput
                name="audifonos_con_mic"
                value={audifonos_con_mic}
                handleCheckboxes={handleCheckboxes('technicals_attributes')}
                label="Audifonos con microfono"
                disabled={false}
            />
            {audifonos_input}
            <CheckboxInput
                name="internet"
                value={internet}
                handleCheckboxes={handleCheckboxes('technicals_attributes')}
                label="Conectividad a internet"
                disabled={false}
            />
            <div className="form-buttons">
                <button className='btn form-btn orange' onClick={nextStep}>Siguiente</button>
                <button className='btn form-btn orange' onClick={prevStep}>Atras</button>
            </div>
            </>
        )
    }
}

export default TechnicalForm
