import React, { Component } from 'react'
import CheckboxInput from '../CheckboxInput'
import TextInput from '../TextInput';

export class MediaForm extends Component {    
    render() {
        const {handleChange, handleCheckboxes, nextStep, prevStep} = this.props
        const {media, advertising_other, advertising_status} = this.props.values
        const {television, radio, prensa, redesSociales, mediosDigitales, otro} = this.props.values.advertising_types_attributes

        const media_other_input = (otro) ? <TextInput
            name="advertising_other"
            value={advertising_other}
            handleChange={handleChange}
            placeholder="Otro tipo de Medios"
            label="Otro tipo de Medios"
        /> : <></>
            
        return (
            <>
            <span className="card-title center-align">Medios y/o publicidad</span>
            <h6 className="form-title">¿Tiene prevista hacer alguna acción publicitaria para su actividad?</h6>
            <div className="form-radio">
                <label>
                    <input className="with-gap" name="advertising_status" value="1" type="radio" checked={advertising_status === '1'} onChange={handleChange} />
                    <span>Si</span>
                </label>
            </div>
            <div className="form-radio">
                <label>
                    <input className="with-gap" name="advertising_status" value="0" type="radio" checked={advertising_status === '0'} onChange={handleChange} />
                    <span>No</span>
                </label>
            </div>
            <h6 className="form-title">Por favor marque a continuación todos los medios que tiene previstos usar para la campaña publicitaria de su actividad</h6>
            <CheckboxInput
                name="television"
                value={television}
                handleCheckboxes={handleCheckboxes('advertising_types_attributes')}
                label="Televisión"
                disabled={advertising_status === '0'}
            />
            <CheckboxInput
                name="radio"
                value={radio}
                handleCheckboxes={handleCheckboxes('advertising_types_attributes')}
                label="Radio"
                disabled={advertising_status === '0'}
            />
            <CheckboxInput
                name="prensa"
                value={prensa}
                handleCheckboxes={handleCheckboxes('advertising_types_attributes')}
                label="Prensa"
                disabled={advertising_status === '0'}
            />
            <CheckboxInput
                name="redesSociales"
                value={redesSociales}
                handleCheckboxes={handleCheckboxes('advertising_types_attributes')}
                label="Redes sociales"
                disabled={advertising_status === '0'}
            />
            <CheckboxInput
                name="mediosDigitales"
                value={mediosDigitales}
                handleCheckboxes={handleCheckboxes('advertising_types_attributes')}
                label="Medios digitales"
                disabled={advertising_status === '0'}
            />
            <CheckboxInput
                name="otro"
                value={otro}
                handleCheckboxes={handleCheckboxes('advertising_types_attributes')}
                label="Otro"
                disabled={advertising_status === '0'}
            />
            {media_other_input}
            <h6 className="form-title">Por favor marque a continuación todos los medios que tiene previstos usar para la campaña publicitaria de su actividad</h6>
            <div className="form-radio">
                <label>
                    <input className="with-gap" type="radio" checked={advertising_status === '1'} disabled={advertising_status === '0'}/>
                    <span>Si</span>
                </label>
            </div>
            <TextInput
                name="media"
                value={media}
                handleChange={handleChange}
                placeholder="Indique los medios que se encontraran presente."
                label="Medios"
                disabled={advertising_status === '0'}
            />
            <div className="form-buttons">
                <button className='btn form-btn orange' onClick={nextStep}>Siguiente</button>
                <button className='btn form-btn orange' onClick={prevStep}>Atras</button>
            </div>
            </>
        )
    }
}

export default MediaForm
