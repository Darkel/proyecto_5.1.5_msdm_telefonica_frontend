import React, { Component } from 'react'
import TextInput from '../TextInput';
import SelectInput from '../SelectInput';

export class ActivityForm extends Component {
    render() {
        const {handleChange, nextStep, prevStep} = this.props
        const {activity_name, objective, description, activity_type, activity_other} = this.props.values
        const activity_other_input = (activity_type === 'Otro') ? <TextInput
            name="activity_other"
            value={activity_other}
            handleChange={handleChange}
            placeholder="Otro tipo de Actividad"
            label="Otro tipo de actividad"
        /> : <></>
        return (
            <>
                <span className="card-title center-align">Datos de la actividad</span>
                <TextInput
                    name="activity_name"
                    value={activity_name}
                    handleChange={handleChange}
                    placeholder="Curso de Programación"
                    label="Nombre *"
                />
                <TextInput
                    name="objective"
                    value={objective}
                    handleChange={handleChange}
                    placeholder="Capacitacion a publico general"
                    label="Objetivo *"
                />
                <div className="row">
                    <div className="input-field col s12">
                    <textarea id="description" name="description" placeholder="Descripcion de la Actividad." onChange={handleChange} defaultValue={description} className="materialize-textarea" data-length="120"></textarea>
                    <label htmlFor="description" className="active">Descripción *</label>
                    </div>
                </div>
                <SelectInput
                    name="activity_type"
                    value={activity_type}
                    values={["Formación",
                        "Investigación y divulgación",
                        "Desarrollo de contenidos",
                        "Actividad recreativa",
                        "Cultural",
                        "Comunitaria",
                        "Otro"]}
                    handleChange={handleChange}
                    label="Tipo de actividad"
                />
                {activity_other_input}
                <div className="form-buttons">
                    <button className='btn form-btn orange' onClick={nextStep}>Siguiente</button>
                    <button className='btn form-btn orange' onClick={prevStep}>Atras</button>
                </div>
            </>
        )
    }
}

export default ActivityForm
