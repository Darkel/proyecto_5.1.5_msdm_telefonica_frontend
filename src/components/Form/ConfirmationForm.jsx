import React, { Component } from 'react'

export class ConfirmationForm extends Component {
    render() {
        const {nextStep, prevStep} = this.props
        return (
            <div>
                <div className="form-buttons">
                    <button className='btn form-btn orange' onClick={nextStep}>Siguiente</button>
                    <button className='btn form-btn orange' onClick={prevStep}>Atras</button>
                </div>
            </div>
        )
    }
}

export default ConfirmationForm
