import React, { Component } from 'react'
import TextInput from '../TextInput';

export class OrganizationForm extends Component {
    render() {
        const {handleChange, nextStep, prevStep} = this.props
        const {organization_name, address, rif, sector, position} = this.props.values
        return (
            <>
                <span className="card-title center-align">Datos de la organización</span>
                <TextInput
                    name="organization_name"
                    value={organization_name}
                    handleChange={handleChange}
                    placeholder="Academia Hack"
                    label="Nombre *"
                />
                <TextInput
                    name="address"
                    value={address}
                    handleChange={handleChange}
                    placeholder="Torre la Primera"
                    label="Dirección *"
                />
                <TextInput
                    name="rif"
                    value={rif}
                    handleChange={handleChange}
                    placeholder="J-00.000.000"
                    label="RIF *"
                />
                <TextInput
                    name="sector"
                    value={sector}
                    handleChange={handleChange}
                    placeholder="Educación Digital"
                    label="Área o sector *"
                />
                <TextInput
                    name="position"
                    value={position}
                    handleChange={handleChange}
                    placeholder="Estudiante"
                    label="Cargo que ocupa en la organización"
                />
                <div className="form-buttons">
                    <button className='btn form-btn orange' onClick={nextStep}>Siguiente</button>
                    <button className='btn form-btn orange' onClick={prevStep}>Atras</button>
                </div>
            </>
        )
    }
}

export default OrganizationForm
