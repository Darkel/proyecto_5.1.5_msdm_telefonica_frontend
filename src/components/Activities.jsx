import React from 'react'
import ActivityOne from '../assets/images/activity1.PNG'
import ActivityTwo from '../assets/images/activity2.PNG'
import ActivityThree from '../assets/images/activity3.PNG'

export default function Activities() {
    return (
        <div className="row container" id='activities'>
            <div className="activity-title center-align white-text">
                <h3>Actividades realizadas en Mega Sala Digital Movistar</h3>
                <h5 style={{color: "#132A3E"}}>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</h5>
            </div>
            <div className="col s12 m6 l4">
            <div className="card">
                <div className="card-image waves-effect waves-block waves-light">
                    <img className="activator" src={ActivityOne} alt="actividad"/>
                </div>
                <div className="card-content">
                    <span className="card-title activator grey-text text-darken-4">Actividad 1<i className="material-icons right">more_vert</i></span>
                </div>
                <div className="card-reveal">
                    <span className="card-title grey-text text-darken-4">Big Data<i className="material-icons right">close</i></span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. In rem ipsum obcaecati, soluta officiis perspiciatis ab autem magni, similique quo vero accusantium, harum natus aut cupiditate! Molestiae veritatis amet hic?</p>
                </div>
            </div>
            </div>
            <div className="col s12 m6 l4">
            <div className="card">
                <div className="card-image waves-effect waves-block waves-light">
                    <img className="activator" src={ActivityTwo} alt="actividad"/>
                </div>
                <div className="card-content">
                    <span className="card-title activator grey-text text-darken-4">Actividad 2<i className="material-icons right">more_vert</i></span>

                </div>
                <div className="card-reveal">
                    <span className="card-title grey-text text-darken-4">Experiencia Pro Futuro<i className="material-icons right">close</i></span>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia quibusdam quasi nostrum officiis, esse est soluta laboriosam hic et sint ut perspiciatis suscipit iure totam, beatae omnis, ad voluptate ex?</p>
                </div>
            </div>
            </div>
            <div className="col s12 m6 l4">
            <div className="card">
                <div className="card-image waves-effect waves-block waves-light">
                    <img className="activator" src={ActivityThree} alt="actividad"/>
                </div>
                <div className="card-content">
                    <span className="card-title activator grey-text text-darken-4">Actividad 3<i className="material-icons right">more_vert</i></span>
                </div>
                <div className="card-reveal">
                    <span className="card-title grey-text text-darken-4">Taller "Programando"<i className="material-icons right">close</i></span>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto numquam maiores cum delectus quis vitae nisi nulla quaerat iste, quidem, consequatur ex neque est perspiciatis? Deserunt dolorum suscipit consequuntur possimus?</p>
                </div>
            </div>
            </div>
        </div>
    )
}
