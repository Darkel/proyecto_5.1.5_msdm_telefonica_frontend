import React from 'react'

export default function Footer() {
    return (
        <footer className="page-footer" style={{backgroundColor: "#132A3E"}}>
          <div className="container">
            <div className="row">
              <div className="col l6 s12">
                <h5 className="white-text">Mega Sala Digital Movistar</h5>
                <p className="grey-text text-lighten-4">Desarrollado por Henry Yu.</p>
              </div>
              <div className="col l4 offset-l2 s12">
                <ul>
                  <li><a className="grey-text text-lighten-3" target="_blank" href="https://www.fundaciontelefonica.com.ve/" rel="noopener noreferrer">Fundación Telefonica</a></li>
                  <li><a className="grey-text text-lighten-3" target="_blank" href="https://legal.fundaciontelefonica.com/aviso-legal-ve/" rel="noopener noreferrer">Aviso Legal</a></li>
                  <li><a className="grey-text text-lighten-3" target="_blank" href="https://legal.fundaciontelefonica.com/politica-de-privacidad-ve/" rel="noopener noreferrer">Politica de Privacidad</a></li>
                  <li><a className="grey-text text-lighten-3" target="_blank" href="https://www.fundaciontelefonica.com.ve/conocenos/contacto/" rel="noopener noreferrer">Contacto</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div className="footer-copyright" style={{backgroundColor: 'rgba(51,51,51,0.4)'}}>
            <div className="container">
            © 2010-2020 Fundación Telefónica
            <a className="grey-text text-lighten-4 right" target="_blank" href="http://www.telefonica.com/" rel="noopener noreferrer">Telefonica</a>
            </div>
          </div>
        </footer>
    )
}
