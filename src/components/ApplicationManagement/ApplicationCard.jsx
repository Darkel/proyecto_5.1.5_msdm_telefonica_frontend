import React, { Component } from 'react'
import {Link} from 'react-router-dom'

export class ApplicationCard extends Component {
    
    render() {
        const {organization, activity, id, application_status} = this.props
        return (
            <div className="row">
                <div className="col s12">
                    <Link to={`/application_management/show/${id}`} className='black-text'>
                        <div className="card hoverable">
                            <div className="card-content">
                                <i className={`material-icons right medium ${application_status[1]}`}>{application_status[0]}</i>
                                <span className="card-title">{id}. {activity}</span>
                                <p>{organization}</p>
                            </div>
                        </div>
                    </Link>
                </div>
            </div>
        )
    }
}

export default ApplicationCard

// style={{position: 'absolute', right: 24, top: 24}}