import React, { Component } from 'react'
import M from 'materialize-css';
import ApplicantShow from './ApplicantShow';
import OrganizationShow from './OrganizationShow';
import ActivityShow from './ActivityShow';
import ReservationShow from './ReservationShow';
import TechnicalShow from './TechnicalShow';
import SurveyShow from './SurveyShow';
import MediaShow from './MediaShow';
import TextInput from '../TextInput'
import axios from 'axios';
import ConflictApplication from './ConflictApplication';

export class ApplicationShowCard extends Component {
    state = {
        reasons: '',
        petitionApproved: false,
        petitionRejected: false
    }

    handleApprovedSubmit = (e) => {
        e.preventDefault()
        axios.patch(`https://sheltered-spire-75633.herokuapp.com/application_management/${this.props.id}`, 
            {
                application_status: 'Aprobado'
            }, 
            {
                headers: {
                    'token-type': localStorage.getItem('token-type'),
                    client: localStorage.getItem('client'),
                    uid: localStorage.getItem('uid'),
                }
            }
        )
        .then(res => {
            console.log(res)
            this.setState({petitionApproved: true})
        })
        .catch((error) => {
            console.log(error)
            this.setState({petitionApproved: error})
        })
    }

    handleRejectSubmit = (e) => {
        e.preventDefault()
        axios.patch(`https://sheltered-spire-75633.herokuapp.com/application_management/${this.props.id}`, 
            {
                application_status: 'Rechazado',
                reasons: this.state.reasons
            }, 
            {
                headers: {
                    'token-type': localStorage.getItem('token-type'),
                    client: localStorage.getItem('client'),
                    uid: localStorage.getItem('uid'),
                }
            }
        )
        .then(res => {
            console.log(res)
            this.setState({petitionRejected: true})
        })
        .catch((error) => {
            console.log(error)
            this.setState({petitionRejected: error})
        })
    }


    handleChange = (e) => {
        this.setState({[e.target.name]: e.target.value })
    }

    componentDidMount() {
        var tabs = document.querySelectorAll('.tabs');
        M.Tabs.init(tabs, {});
        var dropdown = document.querySelectorAll('.dropdown-trigger');
        M.Dropdown.init(dropdown, {
            constrainWidth: false,
            alignment: 'right'
        });
        var modal = document.querySelectorAll('.modal');
        M.Modal.init(modal, {});
        var tooltipped = document.querySelectorAll('.tooltipped');
        M.Tooltip.init(tooltipped, {});
    }

    render() {
        const application = this.props.applications[this.props.id - 1]
        const {reasons, petitionApproved, petitionRejected} = this.state
        const {reservation_dates} = application
        const {usage_type} = application

        const applications = this.props.applications.filter(function(applicationCompare, index){
            for (var show in reservation_dates) {
                for (var key in applicationCompare.reservation_dates){
                    if (applicationCompare.reservation_dates[key].date !== reservation_dates[show].date || applicationCompare.id === application.id){
                        return false;
                    } else {
                        if (applicationCompare.usage_type === 'Todo el día'){
                            return true;
                        } else if (applicationCompare.usage_type === usage_type){
                            return true;
                        } else if (usage_type === 'Todo el día'){
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            }
            return false;
        })

        const approvedApplications = applications.filter(application => application.application_status === 'Aprobado')
        
        const conflictApplication = approvedApplications.length > 0 ?
        approvedApplications.map((application, index) => {
            return <ConflictApplication key={index} application = {application}/>                     
        })
        :
        applications.map((application, index) => {
            return <ConflictApplication key={index} application = {application}/>                     
        })

        return (
            <>
            {
                petitionApproved ? 
                <center>
                    <i className='material-icons green-text' style={{fontSize: 128}}>check_circle_outline</i>
                    <h6 className="form-title">Solicitud Aprobada exitosamente!</h6>
                </center>
                :
                <></>
            }
            {
                petitionRejected ? 
                <center>
                    <i className='material-icons green-text' style={{fontSize: 128}}>block</i>
                    <h6 className="form-title">Solicitud Rechazada exitosamente!</h6>
                </center>
                :
                <></>
            }
            { application.application_status === null ? 
            <div className="application-status">
                <h6 style={{margin: 10}}>Estado de Solicitud: </h6>
                <i className="material-icons tooltipped orange-text" data-position="right" data-tooltip="Pendiente">access_time</i>
            </div> : <></>
            }
            { application.application_status === 'Aprobado' ? 
            <div className="application-status">
                <h6 style={{margin: 10}}>Estado de Solicitud: </h6>
                <i className="material-icons tooltipped green-text" data-position="right" data-tooltip="Aprobado">done</i>
            </div> : <></>
            }
            { application.application_status === 'Rechazado' ? 
            <div className="application-status">
                <h6 style={{margin: 10}}>Estado de Solicitud: </h6>
                <i className="material-icons tooltipped red-text" data-position="right" data-tooltip="Rechazado">close</i>
            </div> : <></>
            }
            <div className="card">
                <div className="card-tabs">
                    <ul className="tabs blue">
                        <li className="tab"><a className="active" href="#solicitante">Solicitante</a></li>
                        <li className="tab"><a href="#organizacion">Organización</a></li>
                        <li className="tab"><a href="#actividad">Actividad</a></li>
                        <li className="tab"><a href="#medios">Medios</a></li>
                        <li className="tab"><a href="#reserva">Reserva</a></li>
                        <li className="tab"><a href="#tecnicos">Técnicos</a></li>
                        <li className="tab"><a href="#adicional">Adicional</a></li>
                    </ul>
                </div>
                <button className='btn right orange dropdown-trigger waves-effect waves-light' style={{position: 'absolute', right: 0}} data-target='acciones' disabled={application.application_status === 'Aprobado' || application.application_status === 'Rechazado'}>
                    <i className="material-icons left">arrow_drop_down</i>acciones
                </button>
                
                <div className="card-content grey lighten-4" style={{paddingTop:36}}>
                    <div id="solicitante">
                        <ApplicantShow applicant = {application.applicant}/>
                    </div>
                    <div id="organizacion">
                        <OrganizationShow organization = {application.organization}/>
                    </div>
                    <div id="actividad">
                        <ActivityShow activity = {application.activity}/>
                    </div>
                    <div id="medios">
                        { application.advertising_status === 1 ?  
                        <MediaShow advertising = {application.advertising}/>
                            : 
                        <>Esta aplicación no tendra medios y/o publicidad</>}
                    </div>
                    <div id="reserva">
                        <ReservationShow 
                            reservation_dates = {application.reservation_dates}
                            room_type = {application.room_type}
                            start_time = {application.start_time}
                            end_time = {application.end_time}
                            usage_type = {application.usage_type}
                        />
                    </div>
                    <div id="tecnicos">
                        <TechnicalShow technicals={application.technicals} />
                    </div>
                    <div id="adicional">
                        <SurveyShow survey={application.survey} />
                    </div>
                </div>
            </div>
            <ul id='acciones' className='dropdown-content'>
                <li><a className='green-text modal-trigger disabled' href="#aprobar"><i className="material-icons left">done</i>Aprobar Solicitud</a></li>
                <li><a className='red-text modal-trigger' href="#rechazar"><i className="material-icons left">close</i>Rechazar Solicitud</a></li>
            </ul>
            <div id="aprobar" className="modal">
                <div className="modal-content">
                <h4>Aprobar solicitud</h4>
                {
                    approvedApplications.length > 0 ? 
                    <p className='center-align'>Esta solicitud no puede ser aceptada.
                    <br></br>
                    <b>Las siguientes solicitudes ya se encuentran aprobadas. Por favor, rechaze la solicitud.</b></p>
                    :
                    <p className='center-align'>¿Esta seguro de aprobar esta solicitud?
                    <br></br>
                    <b>Las siguientes solicitudes no podran ser aprobadas.</b></p>
                }
                <center>
                    {conflictApplication}
                </center>                
                </div>
                <div className="modal-footer">
                <a href="#!" className="modal-close waves-effect waves-green btn-flat red-text"><i className="material-icons left">close</i>Cancelar</a>
                <a href="#!" className="modal-close waves-effect waves-green btn-flat green-text" disabled={approvedApplications.length > 0} onClick={this.handleApprovedSubmit}><i className="material-icons left">done</i>Aceptar</a>
                </div>
            </div>
            
            <div id="rechazar" className="modal">
                <div className="modal-content">
                <h4>Rechazar solicitud</h4>
                <p className='center-align'>¿Esta seguro de aprobar esta solicitud?</p>
                </div>
                <TextInput
                    name="reasons"
                    value={reasons}
                    handleChange={this.handleChange}
                    placeholder="Indique la razon por la cual se cancelerara la solicitud"
                    label="Razon"
                />
                <div className="modal-footer">
                <a href="#!" className="modal-close waves-effect waves-green btn-flat red-text"><i className="material-icons left">close</i>Cerrar</a>
                <a href="#!" className="modal-close waves-effect waves-green btn-flat green-text" disabled={reasons === ''} onClick={this.handleRejectSubmit}><i className="material-icons left">block</i>Rechazar</a>
                </div>
            </div>
            </>
        )
    }
}

export default ApplicationShowCard
