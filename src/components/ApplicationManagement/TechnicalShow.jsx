import React, { Component } from 'react'

export class TechnicalShow extends Component {
    render() {
        const {technicals} = this.props
        return (
            <center>
            <table className='card' style={{width:'initial'}}>
                <tbody>
                    {
                        technicals.map((technical, index)=> {
                            if (technical.quantity){
                                return (
                                    <tr key={index} >
                                        <th>{technical.name}</th> 
                                        <td>Cantidad: {technical.quantity}</td> 
                                    </tr>
                                )
                            } else {
                                return(
                                    <tr key={index}>
                                        <th colSpan='2'>{technical.name}</th> 
                                    </tr>
                                )
                            }
                        
                        })
                    }
                </tbody>
            </table>
            </center>
        )
    }
}

export default TechnicalShow
