import React, { Component } from 'react'
import M from 'materialize-css';
import ApplicationCard from './ApplicationCard';

export class ApplicationsCard extends Component {
    componentDidMount() {
        var elems = document.querySelectorAll('.tabs');
        M.Tabs.init(elems, {});
    }

    render() {
        const {applications} = this.props
        const pendingCard = applications.map(application => {
            if(application.application_status == null){
                return (
                <ApplicationCard 
                    key={application.id}
                    id={application.id}
                    activity={application.activity.name}
                    organization={application.organization.name}
                    application_status={['access_time', 'orange-text']}
                />
            )} else {
                return false
            }
        })
        const approvedCard = applications.map(application => {
            if(application.application_status === 'Aprobado'){
            return (
                <ApplicationCard 
                    key={application.id}
                    id={application.id}
                    activity={application.activity.name}
                    organization={application.organization.name}
                    application_status={['done', 'green-text']}
                />
            )} else {
                return false
            }
        })
        const rejectedCard = applications.map(application => {
            if(application.application_status === 'Rechazado'){
            return (
                <ApplicationCard 
                    key={application.id}
                    id={application.id}
                    activity={application.activity.name}
                    organization={application.organization.name}
                    application_status={['block', 'red-text']}
                />
            )} else {
                return false
            }
        })
        return (
            <div className="card">
                <div className="card-tabs">
                    <ul className="tabs blue">
                        <li className="tab"><a className="active" href="#pendientes">Pendientes</a></li>
                        <li className="tab"><a href="#aprobadas">Aprobadas</a></li>
                        <li className="tab"><a href="#rechazadas">Rechazadas</a></li>
                    </ul>
                </div>
                <div className="card-content grey lighten-4">
                    <div id="pendientes">{pendingCard.filter(card => typeof card === 'object').length > 0 ? pendingCard : 'No hay solicitudes pendientes'}</div>
                    <div id="aprobadas">{approvedCard.filter(card => typeof card === 'object').length > 0 ? approvedCard : 'No hay solicitudes aprobadas'}</div>
                    <div id="rechazadas">{rejectedCard.filter(card => typeof card === 'object').length > 0 ? rejectedCard : 'No hay solicitudes rechazadas'}</div>
                </div>
            </div>
        )
    }
}

export default ApplicationsCard
