import React, { Component } from 'react'

export class OrganizationShow extends Component {
    render() {
        const {organization} = this.props
        return (
            <center>
            <table style={{width: 'initial'}} className='card'>
                <tbody>
                    <tr>
                        <th>Nombre de la Organización</th>
                        <td>{organization.name}</td>
                    </tr>
                    <tr>
                        <th>Dirección</th>
                        <td>{organization.address}</td>
                    </tr>
                    <tr>
                        <th>RIF</th>
                        <td>{organization.rif}</td>
                    </tr>
                    <tr>
                        <th>Sector o Area</th>
                        <td>{organization.sector}</td>
                    </tr>
                    <tr>
                        <th>Cargo del solicitante</th>
                        <td>{organization.position}</td>
                    </tr>
                </tbody>
            </table>
            </center>
        )
    }
}

export default OrganizationShow
