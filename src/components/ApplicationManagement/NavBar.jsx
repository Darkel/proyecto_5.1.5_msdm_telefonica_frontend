import React, { Component } from 'react'
import {Link, Redirect} from 'react-router-dom'
import M from 'materialize-css';
import Axios from 'axios';

export class NavBar extends Component {
    state = {
        redirect: false
    }
    componentDidMount() {
        var elems = document.querySelectorAll('.sidenav');
        M.Sidenav.init(elems, {});
    }

    setRedirect = () => {
        this.setState({
            redirect: true
        })
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/' />
        }
    }

    logoutFunction = () => {
        Axios.delete('https://sheltered-spire-75633.herokuapp.com/auth/sign_out', 
            {
                headers: {
                    'access-token': localStorage.getItem('access-token'),
                    client: localStorage.getItem('client'),
                    uid: localStorage.getItem('uid'),
                }
            }
        )
        .then(() => {
            localStorage.removeItem('access-token');
            localStorage.removeItem('client');
            localStorage.removeItem('uid');
            this.setRedirect()
        })
        .catch((error) => {
            console.log(error)
        })
    }

    render() {
        return (
            <header>
                {this.renderRedirect()}
                <nav>
                <div className="nav-wrapper blue">
                    <div className="nav-wrapper">
                        <a href="#sidenav" data-target="slide-out" className="sidenav-trigger"><i className="material-icons white-text large">menu</i></a>
                        <span className="brand-logo center truncate">Administración MSDM</span>
                        <ul id="nav-mobile" className="right hide-on-med-and-down">
                            <li><a href='#logout' className="waves-effect waves-light btn orange" onClick={this.logoutFunction}><i className="material-icons left">input</i>Cerrar Sesión</a></li>
                        </ul>
                        </div>
                    </div>
                </nav>
                <ul id="slide-out" className="sidenav sidenav-fixed blue-grey darken-4">
                    <li><div className="user-view">
                        <span className="white-text name">Usuario</span>
                    </div></li>
                    <li><Link to='/application_management/' className="white-text waves-effect sidenav-close"><i className="blue-text material-icons">book</i>Solicitudes</Link></li>
                    <li><Link to='/application_management/dashboard' className="white-text waves-effect sidenav-close"><i className="blue-text material-icons">dashboard</i>Dashboard de Solicitudes</Link></li>
                    <li><div className="divider grey darken-3"></div></li>
                    <li><a href="#!" className="subheader white-text">Funcionabilidades no disponibles</a></li>
                    <li><a href="#!" className="grey-text"><i className="grey-text material-icons">today</i>Registro de actividades</a></li>
                    <li><a href="#!" className="grey-text"><i className="grey-text material-icons">assignment_ind</i>Registro de participantes</a></li>
                </ul>
            
            </header>
        )
    }
}

export default NavBar
