import React, { Component } from 'react'

export class ReservationShow extends Component {
    render() {
        const {reservation_dates, room_type, start_time, end_time, usage_type} = this.props
        return (
            <div>
            <center>
            <table className='card' style={{width: 'initial'}}>
                <tbody>
                    <tr>
                        <th colSpan='2'>Tipo de sala a usar</th>
                        <td colSpan='2'>{room_type}</td>
                    </tr>
                    <tr>
                        <th colSpan='2'>Bloque horario</th>
                        <td colSpan='2'>{usage_type}</td>
                    </tr>
                    <tr>
                        <th>Hora de inicio</th>
                        <td>{start_time}</td>
                        <th>Hora de culminación</th>
                        <td>{end_time}</td>
                    </tr>
                    <tr>
                        <th>Dia/Dias</th>
                        <td>{reservation_dates.map((date, index)=>{
                            return (<p key={index}>{date.date}</p>)
                        })}</td>
                    </tr>
                </tbody>
            </table>
            </center>
            </div>
        )
    }
}

export default ReservationShow
