import React, { Component } from 'react'
import {Link} from 'react-router-dom'

export class ConflictApplication extends Component {
    render() {
        const {activity, id} = this.props.application
        return (
            <Link to={`/application_management/show/${id}`} className='modal-close'>
                <div className='chip'>
                    {activity.name}
                </div>
            </Link>
        )
    }
}

export default ConflictApplication
