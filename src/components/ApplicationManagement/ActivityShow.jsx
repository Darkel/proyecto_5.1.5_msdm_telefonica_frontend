import React, { Component } from 'react'

export class ActivityShow extends Component {
    render() {
        const {activity} = this.props
        return (
            <>
            <center>
            <table className='card'>
                <tbody>
                    <tr>
                        <th>Nombre de la actividad</th>
                        <td>{activity.name}</td>
                    </tr>
                    <tr>
                        <th>Objetivo</th>
                        <td>{activity.objective}</td>
                    </tr>
                    <tr>
                        <th>Tipo de actividad</th>
                        <td>{activity.activity_type}</td>
                    </tr>
                    {
                        (activity.activity_type === 'Otro') ?
                        <tr>
                            <th>Otro tipo de Actividad:</th>
                            <td>{activity.activity_other}</td>
                        </tr> 
                        :
                        <></>
                    }
                </tbody>
            </table>
            </center>
            <div className="row">
                <div className="col s12 card">
                    <div className="card-content">
                        <span className="card-title" style={{fontSize: 22}}>Descripción de la actividad</span>
                        <p>{activity.description}</p>
                    </div>
                </div>
            </div>
            </>
        )
    }
}

export default ActivityShow
