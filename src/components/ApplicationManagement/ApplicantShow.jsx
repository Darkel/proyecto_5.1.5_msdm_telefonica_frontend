import React, { Component } from 'react'

export class ApplicantShow extends Component {
    render() {
        const {applicant} = this.props
        return (
            <center>
            <table style={{width: 'initial'}} className='card'>
                <tbody>
                    <tr>
                        <th>Nombre completo</th>
                        <td>{applicant.full_name}</td>
                    </tr>
                    <tr>
                        <th>Cedula de Identidad</th>
                        <td>{applicant.dni}</td>
                    </tr>
                    <tr>
                        <th>Correo Electronico</th>
                        <td>{applicant.email}</td>
                    </tr>
                    <tr>
                        <th>Telefono(Movil)</th>
                        <td>{applicant.cellphone}</td>
                    </tr>
                    <tr>
                        <th>Telefono(Fijo)</th>
                        <td>{applicant.telephone}</td>
                    </tr>
                </tbody>
            </table>
            </center>
        )
    }
}

export default ApplicantShow
