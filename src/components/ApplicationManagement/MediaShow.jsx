import React, { Component } from 'react'

export class MediaShow extends Component {
    state = {
        advertising_other: false
    }
    componentDidMount(){
        this.props.advertising.advertising_types.forEach((profile) => {
            if(profile.name === 'Otro'){
                this.setState({advertising_other: true})
            }
        })
    }

    render() {
        const { advertising } = this.props
        const { advertising_other } = this.state
        const advertising_other_show = advertising_other ? 
        <tr>
            <th>Otros tipos de medio</th>
            <td>{advertising.advertising_other}</td>
        </tr>
        :
        <></>
        return (
            <>
            <center>
            <table className='card'>
                <tbody>
                    <tr>
                        <th>Medios de la campaña publicitaria</th>
                        <td>{advertising.advertising_types.map((advertising_type, index)=>{return(<p key={index}>{advertising_type.name}</p>)})}</td>
                    </tr>
                    {advertising_other_show}
                    <tr>
                        <th>¿Cuales seran los medios presentes en la campaña?</th>
                        <td>{advertising.media}</td>
                    </tr>
                </tbody>
            </table>
            </center>
            </>
        )
    }
}

export default MediaShow
