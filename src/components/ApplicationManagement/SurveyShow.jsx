import React, { Component } from 'react'

export class SurveyShow extends Component {
    state = {
        profile_other: false
    }

    componentDidMount(){
        this.props.survey.profile_types.forEach((profile) => {
            if(profile.name === 'Otro'){
                this.setState({profile_other: true})
            }
        })
    }

    render() {
        const {survey} = this.props
        const {profile_other} = this.state
        const profile_other_show = profile_other ? 
        <tr>
            <th>Otro tipo de perfil</th>
            <td>{survey.profile_other}</td>
        </tr>
        :
        <></>
        return (
            <>
            <center>
            <table className='card'>
                <tbody>
                    <tr>
                        <th>¿Como se entero?</th>
                        <td>{survey.know_type} </td>
                    </tr>
                    {
                        (survey.know_type === 'Otro') ?
                        <tr>
                            <th>¿Cual fue el otro medio que se entero?</th>
                            <td>{survey.know_other}</td>
                        </tr> 
                        :
                        <></>
                    }
                    <tr>
                        <th>Cantidad de personas convocadas</th>
                        <td>{survey.people}</td>
                    </tr>
                    <tr>
                        <th>¿Publica o Privada?</th>
                        <td>{survey.public === 1 ? 'Publico' : 'Privado'}</td>
                    </tr>
                    <tr>
                        <th>Edad de los participantes</th>
                        <td>{survey.age_details.map((age, index)=>{return(<p key={index}>{age.age_range}</p>)})}</td>
                    </tr>
                    <tr>
                        <th>Perfiles de los participantes</th>
                        <td>
                            {survey.profile_types.map((profile, index)=>{
                            return(<p key={index}>{profile.name}</p>
                            )})}
                        </td>
                    </tr>
                    {profile_other_show}
                </tbody>
            </table>
            </center>
            <div className="row">
                <div className="col s12 card">
                    <div className="card-content">
                        <span className="card-title" style={{fontSize: 22}}>Comentario Adicional</span>
                        <p>{survey.commentary}</p>
                    </div>
                </div>
            </div>
            </>
        )
    }
}

export default SurveyShow
