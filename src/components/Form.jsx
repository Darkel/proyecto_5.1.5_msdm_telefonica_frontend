import React, { Component } from 'react';

class Form extends Component {
    render() {
        return (
            <div className="row">
                <form className="col s10 offset-s1">
                <h2>Solicitante</h2>
                <div className="row">
                    <div className="input-field col s12">
                    <input id="full_name" type="text" className="validate"/>
                    <label for="full_name">Nombre Completo</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                    <input id="first_name" type="text" className="validate"/>
                    <label for="first_name">Cedula de identidad</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                    <input id="password" type="text" className="validate"/>
                    <label for="password">Telefono (Movil)</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                    <input id="email" type="text" className="validate"/>
                    <label for="email">Telefono (Fijo)</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                        <input id="email_inline" type="email" className="validate"/>
                        <label for="email_inline">Email</label>
                        <span className="helper-text" data-error="wrong" data-success="right"></span>
                    </div>
                </div>
                <h2>Organización</h2>
                <div className="row">
                    <div className="input-field col s12">
                    <input id="full_name" type="text" className="validate"/>
                    <label for="full_name">Nombre de la Organziación</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                    <input id="first_name" type="text" className="validate"/>
                    <label for="first_name">Dirección</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                    <input id="password" type="text" className="validate"/>
                    <label for="password">RIF</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                    <input id="email" type="text" className="validate"/>
                    <label for="email">Areá o sector</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                        <input id="email_inline" type="email" className="validate"/>
                        <label for="email_inline">Su Cargo en la Organización</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                        <input id="email_inline" type="email" className="validate"/>
                        <label for="email_inline">Su Cargo en la Organización</label>
                    </div>
                </div>
                <h2>Medios</h2>
                <h5>¿Tiene prevista hacer alguna acción publicitaria para su actividad?</h5>
                <div className="row">
                    <label>
                        <input className="with-gap" name="group1" type="radio"  />
                        <span>Si</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input className="with-gap" name="group1" type="radio"  />
                        <span>No</span>
                    </label>
                </div>
                <h5>Por favor marque a continuación todos los medios que tiene previstos usar para la campaña publicitaria de su actividad</h5>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>Televisión</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>Radio</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>Prensa</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>Redes sociales</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>Medios digitales</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>Otro</span>
                    </label>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                        <input id="full_name" type="text" className="validate"/>
                        <label for="full_name">Otro medio</label>
                    </div>
                </div>
                <h5>¿Tiene prevista la presencia de medios el día del evento?</h5>
                <div className="row">
                    <label>
                        <input className="with-gap" name="group1" type="radio" checked/>
                        <span>Si</span>
                    </label>
                </div>
                <h5>Indique cuales:</h5>
                <div className="row">
                    <div className="input-field col s12">
                        <textarea id="textarea2" className="materialize-textarea" data-length="120"></textarea>
                        <label for="textarea2">Medios</label>
                    </div>
                </div>
                <h2>Uso y reserva de fecha</h2>
                <h5>Tipo de sala a usar</h5>
                <div className="row">
                    <label>
                        <input className="with-gap" name="group1" type="radio"  />
                        <span>Sala de computacion</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input className="with-gap" name="group1" type="radio"  />
                        <span>Sala tipo auditorio</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input className="with-gap" name="group1" type="radio"  />
                        <span>Ambas salas</span>
                    </label>
                </div>
                <h5>Periodo de uso de la sala</h5>
                <div className="row">
                    <label>
                        <input className="with-gap" name="group1" type="radio"  />
                        <span>Actividad Unica</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input className="with-gap" name="group1" type="radio"  />
                        <span>Actividad Periodica</span>
                    </label>
                </div>
                <h5>Bloque horario</h5>
                <div className="row">
                    <label>
                        <input className="with-gap" name="group1" type="radio"  />
                        <span>8:00 am a 12:00 pm</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input className="with-gap" name="group1" type="radio"  />
                        <span>1:00 pm a 4:00 pm</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input className="with-gap" name="group1" type="radio"  />
                        <span>Todo el día</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input id="reservation_date" type="text" className="datepicker"/>
                        <label for="reservation_date" >Fecha de la actividad</label>
                    </label>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                    <input id="full_name" type="text" className="validate"/>
                    <label for="full_name">Hora de inicio prevista</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                    <input id="first_name" type="text" className="validate"/>
                    <label for="first_name">Hora de cierre prevista</label>
                    </div>
                </div>
                <h2>Requerimientos tecnicos</h2>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>PC/Laptop</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>SmartTV / Servicio TV Movistar</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>Proyector de video</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>Sonido / Microfono</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>Tabletas</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>Camaras web</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>Audifonos con microfono</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>Conectividad a internet</span>
                    </label>
                </div>
                <h2>Informacion Adicional</h2>
                <h5>¿Cómo se enteró de la Mega Sala Digital? </h5>
                <div className="row">
                    <label>
                        <input className="with-gap" name="group1" type="radio"  />
                        <span>Evento de Fundación Telefónica</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input className="with-gap" name="group1" type="radio"  />
                        <span>Medios de comunicación tradicionales (TV/Radio)</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input className="with-gap" name="group1" type="radio"  />
                        <span>Medios de comunicación digitales (Redes sociales)</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input className="with-gap" name="group1" type="radio"  />
                        <span>Por referencia de otra persona</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input className="with-gap" name="group1" type="radio"  />
                        <span>Otro</span>
                    </label>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                    <input id="full_name" type="text" className="validate"/>
                    <label for="full_name">Otro</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                    <input id="full_name" type="text" className="validate"/>
                    <label for="full_name">Numero de personas convocadas</label>
                    </div>
                </div>
                <h5>Evento abierto al publico</h5>
                <div className="row">
                    <label>
                        <input className="with-gap" name="group1" type="radio"  />
                        <span>Si</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input className="with-gap" name="group1" type="radio"  />
                        <span>No</span>
                    </label>
                </div>
                <h5>Edades de los participantes</h5>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>3 - 6 años</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>7 - 12 años</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>13 - 17 años</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>18 - 28 años</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>29 - 55 años</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>55 - > años</span>
                    </label>
                </div>
                <h5>Perfil de los participantes</h5>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>Público en general</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>Estudiantes</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>Educadores</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>Profesionales</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>Comunidades</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>Investigadores</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>Emprendedores</span>
                    </label>
                </div>
                <div className="row">
                    <label>
                        <input type="checkbox" />
                        <span>Otro</span>
                    </label>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                    <input id="full_name" type="text" className="validate"/>
                    <label for="full_name">Otro</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s12">
                        <textarea id="textarea2" className="materialize-textarea" data-length="120"></textarea>
                        <label for="textarea2">Comentario adicional</label>
                    </div>
                </div>
                </form>
            </div>
        );
    }
}

export default Form;