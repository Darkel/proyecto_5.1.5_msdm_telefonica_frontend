import React, { Component } from 'react';
import AllyOne from '../assets/images/ally (1).jpg'
import AllyTwo from '../assets/images/ally (2).jpg'
import AllyThree from '../assets/images/ally (3).jpg'
import AllyFour from '../assets/images/ally (4).jpg'
import AllyFive from '../assets/images/ally (5).jpg'
import AllySix from '../assets/images/ally (6).jpg'
import AllySeven from '../assets/images/ally (7).jpg'
import AllyEight from '../assets/images/ally (8).jpg'


class Companies extends Component {
    render() {
        return (
            <div className="row container" id='companies'>
                <div className="activity-title center-align">
                    <h3>Compañias que han trabajado con Movistar</h3>
                    <h5 style={{color: "#132A3E"}}>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</h5>
                </div>
                <div className="companies-container">
                    <img className="companies-items" src={AllyOne} alt="Aliados"/>
                    <img className="companies-items" src={AllyTwo} alt="Aliados"/>
                    <img className="companies-items" src={AllyThree} alt="Aliados"/>
                    <img className="companies-items" src={AllyFour} alt="Aliados"/>
                    <img className="companies-items" src={AllyFive} alt="Aliados"/>
                    <img className="companies-items" src={AllySix} alt="Aliados"/>
                    <img className="companies-items" src={AllySeven} alt="Aliados"/>
                    <img className="companies-items" src={AllyEight} alt="Aliados"/>
                </div>
            </div>
        );
    }
}

export default Companies;