import React from 'react'
import {Link} from 'react-router-dom'

export default class Application extends React.Component{
    render(){
        return (
            <div className="col s12 l5">
                <div className="card">
                    <div className="card-image" style={{background: "#132A3E", height: "80px"}}>
                    <span className="card-title">Aplicación</span>
                    </div>
                    <div className="card-content">
                    <p>Si deseas llevar a cabo algún tipo de iniciativa en la MEGA SALA DIGITAL MOVISTAR, te invitamos a formalizar tu requerimiento, donde podrás completar la “Planilla de solicitud de uso de la MEGA SALA DIGITAL MOVISTAR”.</p>
                    </div>
                    <div className="card-action card-action-flex">
                        <Link to="/application" className="waves-effect waves-light btn btn-card-flex orange"><i className="material-icons left">book</i>Solicitud</Link>
                        <a target="_blank" href="https://www.fundaciontelefonica.com.ve/salas-digitales-movistar/programacion-megasala-digital/" className="waves-effect waves-light btn btn-card-flex orange" rel="noopener noreferrer"><i className="material-icons left">event</i>Programacion</a>
                    </div>
                </div>
                <div className="card">
                    <div className="card-image" style={{background: "#132A3E", height: "80px"}}>
                    <span className="card-title">Condiciones de uso</span>
                    </div>
                    <div className="card-content">
                    <p>Para conocer la normativa de uso de la MEGA SALA DIGITAL MOVISTAR</p>
                    </div>
                    <div className="card-action card-action-flex">
                        <a target="_blank" href="https://www.fundaciontelefonica.com.ve/wp-content/uploads/descargas/1475056737-CONDICIONES%20DE%20USO%20Mega%20Sala%20Digital.pdf" className="waves-effect waves-light btn btn-card-flex orange" rel="noopener noreferrer"><i className="material-icons left">chrome_reader_mode</i>Normativas</a>
                    </div>
                </div>
            </div>
        )
    }
}